1.1
// TODO: Step 1 - implement createViewToken to create and return 2L authentication token
if (!this._auth) {
   this._auth = new forge.AuthClientTwoLegged(this._options.consumerKey, this._options.consumerSecret, [ 'viewables:read' ]);
}
const token = await this._auth.authenticate();

res.status(200).json(token);

1.2
<!-- TODO: Step 2 Add references to the viewer CSS -->
<link rel="stylesheet" href="https://developer.api.autodesk.com/modelderivative/v2/viewers/7.*/style.css" type="text/css">

<!-- TODO: Step 2 Add references to the viewer JavaScript -->
<script src="https://developer.api.autodesk.com/modelderivative/v2/viewers/7.*/viewer3D.js"></script>

1.3
// TODO: Step 3 - extend callback to initialize viewer
    const options = {
        env: 'AutodeskProduction',
        getAccessToken: getToken
    };

    Autodesk.Viewing.Initializer(options, () => {
        const container = document.getElementById('viewer-container');

        viewerApp = new Autodesk.Viewing.GuiViewer3D(container);
        Autodesk.Viewing.Document.load(documentId, onDocumentLoadSuccess, onDocumentLoadError);
    });

1.4
    // TODO: Step 4 - find and select viewable
    // Search for geometry
    const viewables = doc.getRoot().search({ 'type': 'geometry' });

    if (viewables.length === 0) {
        console.warn('Document contains no viewables');
        return;
    }
    // Start viewer
    viewerApp.start();
    // Choose first avialable viewable
    viewerApp.loadDocumentNode(doc, viewables[0]).then((model) => {
        this.onItemLoadSuccess(model);
    }).catch((err) => {
        this.onItemLoadError(err);
    });

1.5
// TODO: Step 5 - add function to implement error handler
// TODO: Step 5 - add function to implement item load success handler
// TODO: Step 5 - add function to implement item load error handler
function onDocumentLoadError(errorCode, errorMsg, statusCode, statusText, errors) {
    console.error(`onDocumentLoadError: ${errorCode}`);
}

/**
 * loadDocumentNode resolve handler.
 * Invoked after the model's SVF has been initially loaded.

 * @param {Object} item
 */
function onItemLoadSuccess(item) {
    console.debug('onItemLoadSuccess');
}

/**
 * loadDocumentNode reject handler.
 * @param {Object} error
 */
function onItemLoadError(error) {
    console.error(`onItemLoadError: ${errorCode}`);
}
