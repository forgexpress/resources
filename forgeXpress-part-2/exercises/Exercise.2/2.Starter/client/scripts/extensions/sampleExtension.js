/**
 * Implementation of custom extension
 */
class SampleExtension extends Autodesk.Viewing.Extension {
    constructor(viewer, options) {
        super(viewer, options);
    }

    /**
     * Called when extension is loaded by viewer.
     */
    load() {
        console.debug('Extension loaded');
        return true;
    }

    /**
     * Called when extension is unloaded by the viewer.
     */
    unload() {
        console.debug('Extension unloaded');
        this.destroyToolbar();
        return true;
    }

    /**
     * Virtual method - is called when toolbar is created.
     */
    onToolbarCreated() {
        // TODO: Step 3 - call method to create a toolbar
    }

    createCustomToolbar() {
        // TODO: Step 2 - implement code to create custom toolbar
    }

    destroyToolbar() {
        // TODO: Step 5 - implement function to delete custom toolbar
    }
    onSample(e) {
    // TODO: Step 4 - implement function which is invoked when user clicks on custom button
    }
}

// TODO: Step 1 - register extension