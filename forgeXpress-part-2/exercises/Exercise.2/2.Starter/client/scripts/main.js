
//Replace placeholder below by your encoded model urn
const urn = 'dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6czVfYnVja2V0L0xlZ29IZWxpY29wdGVyLmYzZA';

let viewerApp;
const documentId = `urn:${urn}`;

/**
 * Wait until document is ready and then initialize viewer and display given document.
 */
$(document).ready(() => {
    console.log('Document is ready');
    const options = {
        env: 'AutodeskProduction',
        getAccessToken: getToken
    }
    Autodesk.Viewing.Initializer(options, () => {
        const container = document.getElementById('viewer-container');
        const config = {
        // TODO: Step 1 - add extension name to load automatically
           };

        viewerApp = new Autodesk.Viewing.GuiViewer3D(container, config);
        Autodesk.Viewing.Document.load(documentId, onDocumentLoadSuccess, onDocumentLoadError);
    });
});

/**
 * Obtains authentication token. The information is returned back via callback.
 * @param {callback} callback - callback to pass access token and expiration time.
 */
function getToken(callback) {
    $.ajax({
        url: '/api/auth/viewtoken',
        type: 'POST',
        dataType: 'json',
        success: (response) => {
            callback(response.access_token, response.expires_in);
        },
        error: (response) => {
            console.error(response.statusText);
        }
    });
}

/**
 * Autodesk.Viewing.Document.load() success callback.
 * Proceeds with model initialization.
 * @param {Autodesk.Viewing.Document} doc - loaded document
 */
function onDocumentLoadSuccess(doc, errorsAndWarnings) {
    // Search for geometry
    const viewables = doc.getRoot().search({ 'type': 'geometry' });

    if (viewables.length === 0) {
        console.warn('Document contains no viewables');
        return;
    }
    // Start viewer
    viewerApp.start();
    // Choose first avialable viewable
    viewerApp.loadDocumentNode(doc, viewables[0]).then((model) => {
        onItemLoadSuccess(model);
    }).catch((err) => {
        onItemLoadError(err);
    });
}

/**
 * Autodesk.Viewing.Document.load() failure callback.
 * @param {number} errorCode 
 */
function onDocumentLoadError(errorCode) {
    console.error(`onDocumentLoadError: ${errorCode}`);
}

/**
 * loadDocumentNode resolve handler.
 * Invoked after the model's SVF has been initially loaded.
 
 * @param {Object} item 
 */
function onItemLoadSuccess(item) {
    console.debug('onItemLoadSuccess');
}

/**
 * loadDocumentNode reject handler.
 * @param {Object} error 
 */
function onItemLoadError(error) {
    console.error(`onItemLoadError: ${error}`);
}
