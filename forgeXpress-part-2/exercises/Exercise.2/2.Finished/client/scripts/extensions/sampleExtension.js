/**
 * Implementation of custom extension
 */
class SampleExtension extends Autodesk.Viewing.Extension {
    constructor(viewer, options) {
        super(viewer, options);
    }

    /**
     * Called when extension is loaded by viewer.
     */
    load() {
        console.debug('Extension loaded');
        return true;
    }

    /**
     * Called when extension is unloaded by the viewer.
     */
    unload() {
        console.debug('Extension unloaded');
        this.destroyToolbar();
        return true;
    }

    /**
     * Virtual method - is called when toolbar is created.
     */
    onToolbarCreated() {
        this.createCustomToolbar();
    }

    createCustomToolbar() {
        this._btnSample = new Autodesk.Viewing.UI.Button('SampleExtension.Sample');
        this._btnSample.setIcon('adsk-icon-bug');
        this._btnSample.setToolTip('Sample command');
        this._btnSample.onClick = (e) => {
            this.onSample(e);
        };
        // add button to the group
        const ctrlGroup = new Autodesk.Viewing.UI.ControlGroup('SampleExtension.ControlGroup');

        ctrlGroup.addControl(this._btnSample);
        // add group to main toolbar
        this.viewer.toolbar.addControl(ctrlGroup);
    }

    destroyToolbar() {
        const toolbar = this.viewer.getToolbar(false);

        if (toolbar) {
            const ctrlGroup = toolbar.getControl('SampleExtension.ControlGroup');

            toolbar.removeControl(ctrlGroup);
        }
    }

    /**
     * Called when user clicks on custom button
     * @param {Object} e - event object
     */
    onSample(e) {
        Autodesk.Viewing.Private.HudMessage.displayMessage(this.viewer.container, {
            msgTitleKey: 'forgeXpress',
            messageKey: 'Hello from Sample Extension :-)'
        }, () => {
            // dummy callback so that 'X' is shown
        });
    }
}

// Register extension in the viewer
Autodesk.Viewing.theExtensionManager.registerExtension('SampleExtension', SampleExtension);
