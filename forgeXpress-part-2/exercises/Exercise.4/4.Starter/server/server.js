const express = require('express');
const auth = require('./authService');

const app = express();

app.use('/', express.static(`${__dirname}/../client`));
const authSvc = new auth.AuthService({
    consumerKey: process.env.CONSUMER_KEY,
    consumerSecret: process.env.CONSUMER_SECRET,
});

app.use('/api/auth', authSvc.router);
const port = process.env.PORT || 5000;

process.on('unhandledRejection', (reason,promise) => {

	console.log('Unhandled Rejection at:',reason.stack || reason)

})

app.set('port', port);
app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});
