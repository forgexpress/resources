/**
 * Implementation of custom extension
 */
class SampleExtension extends Autodesk.Viewing.Extension {
    constructor(viewer, options) {
        super(viewer, options);
    }

    /**
     * Called when extension is loaded by viewer.
     */
    load() {
        console.debug('Extension loaded');
        return true;
    }

    /**
     * Called when extension is unloaded by the viewer.
     */
    unload() {
        console.debug('Extension unloaded');
        return true;
    }

    /**
     * Virtual method - is called when toolbar is created.
     */
    onToolbarCreated() {
        this.createToolbar();
    }

    createToolbar() {
        this._btnSample = new Autodesk.Viewing.UI.Button('SampleExtension.Sample');
        this._btnSample.setIcon('adsk-icon-bug');
        this._btnSample.setToolTip('Sample command');
        this._btnSample.onClick = (e) => {
            this.onSample(e);
        };
        // add button to the goup
        const ctrlGroup = new Autodesk.Viewing.UI.ControlGroup('SampleExtension.ControlGroup');

        ctrlGroup.addControl(this._btnSample);
        // add group to main toolbar
        this.viewer.toolbar.addControl(ctrlGroup);
    }

    /**
     * Called when user clicks on custom button
     * @param {Object} e - event object
     */
    onSample(e) {
        // TODO: Step 1 - add code to obtain selected objects
        // TODO: Step 2 - add code to apply theming color to the selection
        // TODO: Step 3 - add code to proces non-leaf nodes
    }
}

// Register extension in the viewer
Autodesk.Viewing.theExtensionManager.registerExtension('SampleExtension', SampleExtension);
