class SamplePanel extends Autodesk.Viewing.UI.DockingPanel {
    constructor(viewer, id, options) {
        super(viewer.container, id, 'Sample', options);
        this.viewer = viewer;
        this.container.classList.add('sample-panel');
        this.container.style.left = '60px';
        this.container.style.top = '40px';
        this.container.style.width = '340px';
        this.container.style.height = '190px';
        this.container.style.position = 'absolute';
        // scroll container
        this.createScrollContainer({
            heightAdjustment: 70,
            left: false,
            marginTop: 0
        });
        // create UI
        const url = `${window.location.href}scripts/extensions/res/samplePanel.html`;

        Autodesk.Viewing.Private.getHtmlTemplate(url, (err, content) => {
            this.onTemplate(err, content);
        });
    }

    toggleVisibility() {
        this.setVisible(!this.isVisible());
    }

    onApply() {
        this.viewer.clearThemingColors(this.viewer.model);
        const propertyName = this._propertyName.val();
        const propertyValue = this._propertyValue.val();

        if ((propertyName.length === 0) || (propertyValue.length === 0)) {
            return;
        }
        this.viewer.model.getObjectTree((instanceTree) => {
            const ids = [];
        // TODO: Step 2 - collect all leaf nodes
        
            const properties = [];

            properties.push(propertyName);
        // TODO: Step 3 - search for objects which contain given property

        // TODO: Step 4 - isolate matching ids

        });
    }
        
 
    onClose() {
        // TODO: Step 5 - clear theming and isolation

    }

    onTemplate(err, content) {
        const tmp = document.createElement('div');

        tmp.innerHTML = content;
        this.scrollContainer.appendChild(tmp.childNodes[0]);
        // TODO: Step 1 - bind controls to the HTML elements    
    }
}
