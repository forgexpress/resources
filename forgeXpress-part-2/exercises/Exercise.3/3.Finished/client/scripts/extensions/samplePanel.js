class SamplePanel extends Autodesk.Viewing.UI.DockingPanel {
    constructor(parentContainer, id, options) {
        super(parentContainer, id, 'Sample', options);
        this.container.classList.add('sample-panel');
        this.container.style.left = '60px';
        this.container.style.top = '40px';
        this.container.style.width = '200px';
        this.container.style.height = '160px';
        this.container.style.position = 'absolute';
        // scroll container
        this.createScrollContainer({
            heightAdjustment: 70,
            left: false,
            marginTop: 0
        });
        // create UI
        const url = `${window.location.href}scripts/extensions/res/samplePanel.html`;

        Autodesk.Viewing.Private.getHtmlTemplate(url, (err, content) => {
            this.onTemplate(err, content);
        });
    }

    toggleVisibility() {
        this.setVisible(!this.isVisible());
    }

    onTemplate(err, content) {
        const tmp = document.createElement('div');

        tmp.innerHTML = content;
        this.scrollContainer.appendChild(tmp.childNodes[0]);
    }
}
