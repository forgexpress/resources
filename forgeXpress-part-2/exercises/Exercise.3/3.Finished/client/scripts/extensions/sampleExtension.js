/**
 * Implementation of custom extension
 */
class SampleExtension extends Autodesk.Viewing.Extension {
    constructor(viewer, options) {
        super(viewer, options);
    }

    /**
     * Called when extension is loaded by viewer.
     */
    load() {
        console.debug('Extension loaded');
        return true;
    }

    /**
     * Called when extension is unloaded by the viewer.
     */
    unload() {
        console.debug('Extension unloaded');
        this.destroyToolbar();
        if (this._panel) {
            this.viewer.removePanel(this._panel);
            this._panel.uninitialize();
            this._panel = null;
        }
        return true;
    }

    onToolbarCreated() {
        this.createToolbar();
    }

    createToolbar() {
        this._btnSample = new Autodesk.Viewing.UI.Button('SampleExtension.Sample');
        this._btnSample.setIcon('adsk-icon-bug');
        this._btnSample.setToolTip('Sample command');
        this._btnSample.onClick = (e) => {
            this.onSample(e);
        };
        // add button to the goup
        const ctrlGroup = new Autodesk.Viewing.UI.ControlGroup('SampleExtension.ControlGroup');

        ctrlGroup.addControl(this._btnSample);
        // add group to main toolbar
        this.viewer.toolbar.addControl(ctrlGroup);
    }

    destroyToolbar() {
        const toolbar = this.viewer.getToolbar(false);

        if (toolbar) {
            const ctrlGroup = toolbar.getControl('SampleExtension.ControlGroup');

            toolbar.removeControl(ctrlGroup);
        }
    }

    /**
     * Called when user clicks on custom button
     * @param {Object} e - event object
     */
    onSample(e) {
        if (!this._panel) {
            this._panel = new SamplePanel(this.viewer.container, 'SampleExtension.SamplePanel');
            this.viewer.addPanel(this._panel);
            this._panel.addVisibilityListener((state) => {
                this._btnSample.setState(state ? Autodesk.Viewing.UI.Button.State.ACTIVE : Autodesk.Viewing.UI.Button.State.INACTIVE);
            });
            this._panel.setVisible(true);
        }
        else {
            this._panel.toggleVisibility();
        }
    }
}

// Register extension in the viewer
Autodesk.Viewing.theExtensionManager.registerExtension('SampleExtension', SampleExtension);
