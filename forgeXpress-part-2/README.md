# Link utili

## Materiale
* [Slide](https://prezi.com/view/pPyG8tkEVuUSMNMXnise/)
* [Files](https://gitlab.com/forgexpress/resources)
* [Recording](https://app.box.com/s/t9eqrnmuwtgdje0prronlmnnbiglpoe1)

## Documentazione
* [Forge](https://forge.autodesk.com/developer/documentation)
* [Stack overflow Forge tag](https://stackoverflow.com/questions/tagged/autodesk-forge)
* [Learn Forge](https://learnforge.autodesk.io/#/)
* [ECMAScript 2019 Language Specification](https://www.ecma-international.org/publications/standards/Ecma-262.htm)
* [Autodesk Viewer CSS](https://developer.api.autodesk.com/modelderivative/v2/viewers/style.css?v=v6.0)
* [Three.js](https://threejs.org/)

## Strumenti
* [Node.JS](https://nodejs.org)
* [VSCode](https://code.visualstudio.com/Download)
* [Git](https://git-scm.com/downloads)
* [Font Awesome](https://astronautweb.co/snippet/font-awesome/)
* [Forge per VSCode](https://forge.autodesk.com/blog/forge-visual-studio-code)

## Approfondimenti
### Autodesk University
* [Git it together](https://www.autodesk.com/autodesk-university/class/Git-It-Together-Introduction-Version-Control-Revit-Users-2018)
* [Turning Days into Minutes: Forge Design Automation at Scale](https://www.autodesk.com/autodesk-university/class/Turning-Days-Minutes-Forge-Design-Automation-Scale-2019)
* [Creating Flexible Offline Workflows Using Autodesk Forge](https://www.autodesk.com/autodesk-university/class/Creating-Flexible-Offline-Workflows-Using-Autodesk-Forge-2018)
* [Advanced Techniques for Using the Forge Viewer](https://www.autodesk.com/autodesk-university/class/Advanced-Techniques-Using-Forge-Viewer-2018)
* [Control and Program a Real Robot by Manipulating Its 3D Model Using Forge](https://www.autodesk.com/autodesk-university/class/Control-and-Program-Real-Robot-Manipulating-Its-3D-Model-Using-Forge-2017)
* [HTML5, CSS, and JavaScript: Why They Are Important to You and How They Fit Together](https://www.autodesk.com/autodesk-university/class/HTML5-CSS-and-JavaScript-Why-They-Are-Important-You-and-How-They-Fit-Together-2014)
### Altro
* [#100DaysOfVanillaJS](https://dev.to/taeluralexis/100daysofvanillajs-what-is-javascript-primitive-data-types-let-var-and-const-4040)
* [Become a git guru](https://www.atlassian.com/git/tutorials)
* [The Node Beginner Book](https://www.nodebeginner.org/)
* [W3 Schools](https://www.w3schools.com/)

